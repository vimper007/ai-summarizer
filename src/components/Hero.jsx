import React from 'react';
import '../App.css';
import logo from '../assets/logo.svg';

const Hero = () => {
  return (
    <header className='w-full flex justify-center flex-col'>
      <nav className='flex justify-between items-center w-full mb-10 pt-5'>
        <img src={logo} alt="logo" className='w-28 object-contain' />
        <button type='button' onClick={() => window.open()} className='black_btn'>GitLab</button>
      </nav>
      <h1 className='head_text'>Summarize Articles with <br className='max-md:hidden' />
        <span className='orange_gradient'>OpenAI GPT-4</span>
      </h1>
      <h2 className='desc mx-auto'>
        Simplify your reading with Summize, an open source article sumamrizer powered by GPT-4, that helps you transform lengthy articles into clear and concise sumamries
      </h2>
    </header>
  )
}

export default Hero